let trainer = {}

trainer.name = "Ash Ketchum"
trainer.age = 10
trainer.friends = {
    hoenn: ["May", "Max"],
    kanto: ["Brock", "Misty"]
}
trainer.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"]

trainer.talk = function () {
    console.log("Pikachu! I choose you!")
}

console.log(trainer)
console.log('Result of dot notation:')
console.log(trainer.name)
console.log('Result of square bracket notation:')
console.log(trainer['pokemon'])

console.log('Result of talk method:')
trainer.talk()

function Pokemon(name, level, health, attack) {
    this.name = name;
    this.level = level;
    this.health = health;
    this.attack = attack;
    this.tackle = function(attacker, target) {
        console.log(attacker.name + " tackled " + target.name)
        let tackledPokemonHealth = target.health - attacker.attack;
        if (tackledPokemonHealth <= 0){
            console.log(target.name +"'s health is now reduced to " + tackledPokemonHealth)
            target.faint();
            target.health = tackledPokemonHealth;
            console.log(target)
        }
        else {
            console.log(target.name +"'s health is now reduced to " + tackledPokemonHealth)
            target.health = tackledPokemonHealth;
            console.log(target)
        }
    }
    this.faint = function(name) {
        console.log(this.name + " has fainted.")
    }
}

let pokemon1 = new Pokemon("Pickachu", 12, 24, 12)
let pokemon2 = new Pokemon("Geodude", 8, 16, 8)
let pokemon3 = new Pokemon("Mewtwo", 100, 200, 100)

console.log(pokemon1)
console.log(pokemon2)
console.log(pokemon3)

attacker = pokemon2
target = pokemon1 


pokemon2.tackle(pokemon2, pokemon1);


pokemon3.tackle(pokemon3, pokemon2)


